const express = require('express');
const cors = require('cors');
const { nanoid } = require('nanoid');
const app = express();

require('express-ws')(app);
const port = 8000;
app.use(cors());

const activeConnection = {};
const savedCoordinates = [];


app.ws('/draw', (ws) => {
    const id = nanoid();
    console.log('client connected id=', id);
    activeConnection[id] = ws;

    ws.send(JSON.stringify({
        type: 'PREV_PIXELS',
        coordinates: savedCoordinates,
    }))

    ws.on('message', msg => {
        const decodedMessage = JSON.parse(msg);
        switch (decodedMessage.type) {
            case 'SEND_PIXEL':
                Object.keys(activeConnection).forEach(id => {
                    const conn = activeConnection[id];
                    savedCoordinates.push(decodedMessage.coordinates);
                    conn.send(JSON.stringify({
                        type: 'NEW_PIXEL',
                        pixelCoordinates: decodedMessage.coordinates,
                    }));
                });
                break
            default:
                console.log('Unknown message: ', decodedMessage.type);
        }
    });

    ws.on('close', () => {
        console.log('client disconnected id=', id);
        delete activeConnection[id];
    });
});

app.listen(port, () => {
    console.log(`Server started on ${port} port`);
});